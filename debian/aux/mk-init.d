#!/bin/sh
awk -F '[ ]?:[ ]?' '
/^Package:/     { pkg = $2; }
/^Description:/ {
  confdir = "debian/sysvinit/conf/" pkg ;
  system("mkdir -p " confdir);
  outfile = confdir "/conf.yaml"
  printf("name: %s\ndescription: %s", pkg, $2) > outfile;
}' < debian/control

for service in axfrdns dnscache tinydns walldns rbldns ; do
	ionit -t debian/sysvinit -c "debian/sysvinit/conf/${service}"
	mv debian/sysvinit/initscript "debian/${service}.init"
	mv debian/sysvinit/servicefile "debian/${service}.service"
done
